import { Component, OnInit } from '@angular/core';

import { Task } from './task';
import { TaskService } from "./task.service";

@Component({
  moduleId: module.id,
  selector: 'td-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit {
  selectedTask: Task;
  tasks: Task[];
  category: string = 'all';

  ngOnInit(): void {
    this.getTasks();
  }

  constructor(private taskService: TaskService) {}

  onSelect(task: Task): void {
    this.selectedTask = task;
  }

  getTasks():  void {
    this.taskService.getTasks().then(tasks => this.tasks = tasks);
  }

  setCategory(category: string) {
    this.category = category;
  }

  updateProgress(task: Task): void {
    this.taskService.update(task)
      .then(() => null);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.taskService.create(name)
      .then(task => {
        this.tasks.push(task);
        this.selectedTask = null;
      });
  }

  deleteTask(task: Task): void {
    this.taskService
      .deleteTask(task.id)
      .then(() => {
        this.tasks = this.tasks.filter(t => t !== task);
        if (this.selectedTask === task) { this.selectedTask = null; }
      });
  }
}

