/**
 * Created by Vedant on 17-03-2017.
 */
export class Task {
  id: number;
  name: string;
  status: boolean;
}
