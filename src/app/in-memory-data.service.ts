import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let tasks = [
      { id: 1, name: 'Clone repository', status: false},
      { id: 2, name: 'Show and display the in progress task!', status: false},
      { id: 3, name: 'Brief about Design Patterns', status: false},
      { id: 4, name: 'Ask for feedback', status: false}
    ];
    return {tasks};
  }
}
