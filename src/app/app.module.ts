import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { TaskDetailComponent } from "./task-detail.component";
import { TaskService } from './task.service'
import { TasksComponent } from './tasks.component';
import { AboutComponent } from './about.component';

@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    RouterModule.forRoot([
      {
        path: 'tasks',
        component: TasksComponent
      },
      {
        path: '',
        redirectTo: '/tasks',
        pathMatch: 'full'
      },
      {
        path: 'detail/:id',
        component: TaskDetailComponent
      },
      {
        path: 'about',
        component: AboutComponent
      }
    ])
  ],
  declarations: [
    AppComponent,
    TaskDetailComponent,
    TasksComponent,
    AboutComponent
  ],
  providers: [
    TaskService
  ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
