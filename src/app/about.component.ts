import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'td-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})

export class AboutComponent {}
