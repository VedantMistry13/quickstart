import { Task } from './task';

export const TASKS: Task[] = [
  { id: 1, name: 'Clone repository', status: false},
  { id: 2, name: 'Show and display the in progress task!', status: false},
  { id: 3, name: 'Brief about Design Patterns in software engineering', status: false},
  { id: 4, name: 'Ask for feedback', status: false}
];
